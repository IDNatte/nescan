export type ScanResults = {
  IPAddress: string,
  Hostname: string,
  PingStats: {
    "delay": number
  },
  ports: number[] | null[]
}