import { writable } from "svelte/store";
import type { ScanResults } from "../../type/scan";


export default writable<ScanResults[]>([])