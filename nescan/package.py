import PyInstaller.__main__
from pathlib import Path


def build_linux():
    PyInstaller.__main__.run(
        [
            f"{Path(__file__).parent.absolute()}/scripts/nescan-linux.spec",
            "--distpath",
            "package-linux",
        ]
    )
