import { writable } from "svelte/store";
import type { NavigationStore } from "../../type/loader";


export default writable<NavigationStore>(null)