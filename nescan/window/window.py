import webview
import os

from nescan.lib.utils.resources import get_resources
from nescan.lib.api.api import WebIPC


def main():
    env = os.environ.get("APP_ENV", default=None)

    match env:
        case "dev":
            webview.create_window(
                "Nescan",
                "http://localhost:5173",
                width=1024,
                height=768,
                min_size=(1024, 768),
                js_api=WebIPC(),
            )
            webview.start(debug=True)

        case "preview":
            webview.create_window(
                "Nescan",
                get_resources("../../../nescan/template/home/index.html"),
                width=1024,
                height=768,
                min_size=(1024, 768),
                js_api=WebIPC(),
            )
            webview.start(debug=True)

        case _:
            webview.create_window(
                "Nescan",
                get_resources("template/index.html"),
                width=1024,
                height=768,
                min_size=(1024, 768),
                js_api=WebIPC(),
            )
            webview.start()
