import concurrent.futures
import ipaddress
import socket

from ping3 import ping


def _scan_host(host, ports):
    try:
        ping_stats = ping(str(host), timeout=1, unit="ms")
        if ping_stats != None:
            try:
                hostname = socket.gethostbyaddr(str(host))[0]
            except socket.herror:
                hostname = "N/A"

            return {
                "IPAddress": str(host),
                "Hostname": hostname,
                "PingStats": {"delay": round(ping_stats, 1)},
                "ports": _port_prober(str(host), ports),
            }
    except socket.error as _:
        pass


def scan_network(ip_range, ports):
    try:
        network = ipaddress.IPv4Network(ip_range, strict=False)
        with concurrent.futures.ThreadPoolExecutor() as executor:
            prober = [
                executor.submit(_scan_host, network, ports) for network in network
            ]
            live_host = [
                prober.result()
                for prober in concurrent.futures.as_completed(prober)
                if prober.result() is not None
            ]

        return sorted(live_host, key=lambda x: x.get("IPAddress"))
    except Exception as e:
        return {"err_info": e}


def _port_prober(ip, ports):
    try:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [executor.submit(_check_port, ip, port) for port in ports]
            open_ports = [
                future.result()
                for future in concurrent.futures.as_completed(futures)
                if future.result() is not None
            ]
    except Exception as e:
        return {"err_info": e}

    return open_ports


def _check_port(ip, port):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(1)
            if s.connect_ex((ip, port)) == 0:
                return port
    except Exception as e:
        return {"err_info": e}
