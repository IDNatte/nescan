from nescan.lib.utils.prober import scan_network


class WebIPC:
    def init(self):
        return {"init": "all initialized...!!"}

    def start_probe(self, ip_range, ports):
        return scan_network(ip_range, ports)
